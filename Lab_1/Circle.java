package Lab_1;

public class Circle {

    public double x;
    public double y;
    public double radius;

    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    public void translate(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double scale(double scaler) {
        this.radius = radius + scaler;
        return radius;
    }

    public double getX() {
        return x;
    }

    public void setX (double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
