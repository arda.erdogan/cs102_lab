package Lab_1;

public class Main_Circle {
    public static void main(String[] args) {
        Circle circle = new Circle();
        circle.setX(-2.06);
        circle.setY(-0.23);
        circle.setRadius(1.33);

        System.out.println(circle.getArea());

        circle.translate(0.0,5.0);
        circle.scale(0.55);

        System.out.println(circle.getArea());

    }
}
