package Lab_2;

public class Car {
    private double odometer;
    private String brand;
    private String color;
    private int gear;
    private double totalNumberOfHoursDriven;

    public Car() {
        odometer = 0.0;
        brand = "Peugeot";
        color = "Black";
        gear = 0;
        totalNumberOfHoursDriven = 0.0;
    }

    public Car(String brand) {
        odometer = 0.0;
        this.brand = brand;
        color = "Black";
        gear = 0;
        totalNumberOfHoursDriven = 0.0;
    }

    public Car(String brand, String color) {
        odometer = 0.0;
        this.brand = brand;
        this.color = color;
        gear = 0;
        totalNumberOfHoursDriven = 0.0;
    }

    public void incrementGear() {
        if (gear < 5)
            gear++;
        else
            System.out.println("Error: The gear value must be in range [0, 5].");
    }

    public void decrementGear() {
        if (gear > 0)
            gear--;
        else
            System.out.println("Error: The gear value must be in range [0, 5].");
    }

    public void drive(double numberOfHoursTravelled, double kmTravelledPerHour) {
        totalNumberOfHoursDriven += numberOfHoursTravelled;
        odometer += numberOfHoursTravelled * kmTravelledPerHour;
    }

    public double getAverageSpeed() {
        return odometer / totalNumberOfHoursDriven;
    }

    public void display() {
        System.out.println("The " + brand + " has color " + color + ".\n" +
                "It has travelled " + odometer + "kms so far.\n" +
                "It is at gear " + gear + ".\n" +
                "The car has been driven for " + totalNumberOfHoursDriven + " hours.\n");
    }

    public double getOdometer() {
        return odometer;
    }

    public String getBrand() {
        return brand;
    }

    public String getColor() {
        return color;
    }

    public int getGear() {
        return gear;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
