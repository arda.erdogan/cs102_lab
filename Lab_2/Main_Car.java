package Lab_2;

public class Main_Car {

    public static void main(String[] args) {
        Car car1 = new Car("Mercedes", "Yellow");
        Car car2 = new Car("Ferrari", "Red");
        Car car3 = new Car("Volkswagen", "Pink");

        car1.display();
        car2.display();
        car3.display();

        car1.drive(5.2, 75);
        car1.incrementGear();
        car1.display();
        /* Test average speed */
        System.out.println("Average speed of Car 1 is " + car1.getAverageSpeed());
        /* Test decrement Gear */
        car1.decrementGear();
        car1.decrementGear();

        car2.drive(4.8, 120);
        car2.incrementGear();
        car2.incrementGear();
        car2.decrementGear();
        car2.incrementGear();
        car2.drive(1.2, 90);
        car2.display();
        /* Test average speed */
        System.out.println("Average speed of Car 2 is " + car2.getAverageSpeed());
        /* Test increment Gear */
        car2.incrementGear();
        car2.incrementGear();
        car2.incrementGear();
        car2.incrementGear();

    }

}
