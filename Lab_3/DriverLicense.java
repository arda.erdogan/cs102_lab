package Lab_3;

public class DriverLicense {

    private String driverName;
    private String bloodType;
    private int expirationDate;
    private int driverID;
    private int limit;
    private double penalty;
    private boolean status;


    public DriverLicense(String driverName, String bloodType,int expirationDate, int driverID, int limit, double penalty, boolean status) {

        this.driverName = driverName;
        this.bloodType = bloodType;
        this.expirationDate = expirationDate;
        this.driverID = driverID;
        this.limit = limit;
        this.penalty = penalty;
        this.status = status;

    }

    public DriverLicense(String driverName, String bloodType, int driverID) {
        this(driverName,bloodType,0,driverID,100,0,true);
    }

    public DriverLicense() {
        this("Arda","B Rh+",0,5257,100,0,true);
    }

    public void setExpirationDate() {
        setStatus();
        if (!status) {
            System.out.println("There are " + expirationDate + " days before your status becomes active.");
        } else {
            System.out.println("You have " + expirationDate + " penalties left to have your account suspended");
        }
        System.out.println("============================================================");
    }

    public void setStatus() {
        if (penalty >= 20) {
            this.status = false;
            System.out.println("License's Status: Suspended");
            System.out.println("==============================");
            this.expirationDate = (int) penalty - 20;
        } else if (penalty < 20) {
            this.status = true;
            System.out.println("License's Status: Active");
            System.out.println("==============================");
            this.expirationDate =  20 - (int) penalty;
        }
    }

    public void decreasePenalty() {
        this.penalty--;
        System.out.println("Current Penalty: " + this.penalty);
        System.out.println("==============================");
        setStatus();
    }

    public void increasePenalty(int penalty) {
        this.penalty += penalty;
        System.out.println("Current Penalty: " + this.penalty);
        System.out.println("==============================");
        setStatus();
    }

    public String toString() {
        System.out.println("==============================");
        return "Driver Name: " + driverName + "\n" +
                "Blood Type: " + bloodType + "\n" +
                "Expiration Date: " + expirationDate + "\n" +
                "Driver ID: " + driverID + "\n" +
                "Penalty: " + penalty + "\n" +
                "Status: " + status;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }


    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public void setDriverID(int driverID) {
        this.driverID = driverID;
    }

    public String getDriverName() {
        return driverName;
    }

    public int getExpirationDate() {
        return expirationDate;
    }

    public String getBloodType() {
        return bloodType;
    }

    public int getDriverID() {
        return driverID;
    }

    public int getLimit() {
        return limit;
    }

    public double getPenalty() {
        return penalty;
    }

    public boolean isStatus() {
        return status;
    }
}
