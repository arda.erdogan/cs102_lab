package Lab_3;

public class Main_DriverLicense {

    public static void main(String[] args) {
        DriverLicense driverLicense1 = new DriverLicense("Lewis Hamilton", "A Rh+",233);

        System.out.println(driverLicense1.toString());
        driverLicense1.increasePenalty(15);
        driverLicense1.decreasePenalty();
        driverLicense1.setExpirationDate();
        driverLicense1.increasePenalty(10);
        driverLicense1.setExpirationDate();
        driverLicense1.decreasePenalty();
        System.out.println(driverLicense1.toString());

        DriverLicense driverLicense2 = new DriverLicense();

        driverLicense2.increasePenalty(29);
        System.out.println(driverLicense2.toString());
        driverLicense2.setExpirationDate();
    }

}
