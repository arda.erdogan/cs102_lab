package Lab_7;

public class Cloth extends Product{
    private String brand;
    private String color;
    private int quantity;

    public Cloth(double taxRate, String color, String brand, double price, String name, int quantity) {
        super(taxRate,price, name);
        this.color = color;
        this.brand = brand;
        this.quantity = quantity;
    }

    public String toString() {
        return super.toString() + "Brand: " + brand +
                "\nColor: " + color + "\nQuantity: " + quantity + "\n";
    }
}
