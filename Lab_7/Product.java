package Lab_7;

public class Product {
    private double taxRate;
    private double price;
    private String name;

    public Product(double taxRate, double price, String name) {
        this.taxRate = taxRate;
        this.price = price;
        this.name = name;
    }

    public Product(double taxRate, String name) {
        this(taxRate,1., name);
    }

    public Product() {
        this(18.,1., "Nike");
    }

    public String toString() {
        return "Name: " + this.name + "\nPrice: " + this.price +
                "\nTax Rate: " + this.taxRate + "\n";
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }
}
